﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Models
{
    public class License
    {
        /// <summary>
        /// Score based on how many props are an exact match. 9/9 is 100% and it goes down in increments from there
        /// </summary>
        public int ConfidenceScore { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime DOB { get; set; }

        public string LicenseNo { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
    }
}
