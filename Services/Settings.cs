﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DealerPolicy.Developer
{
    public class Settings : IDependentService
    {
        private string JsonDB { get; set; }

        public Settings(string _jsonDB)
        {
            JsonDB = _jsonDB;
        }

        public Task<string> GetValueAsync()
        {
            throw new NotImplementedException();
        }

        public string GetDB()
        {
            return JsonDB;
        }
    }
}
