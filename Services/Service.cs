﻿using Services.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace DealerPolicy.Developer
{
    public class Service
    {
        private readonly Settings _settings;

        public Service(IDependentService settings)
        {
            _settings = (Settings)settings;
        }

        public async Task<bool> MyMethodAsync(string value)
        {
            if (value == null)
            {
                value = await _settings.GetValueAsync().ConfigureAwait(false);
            }

            return value != null;
        }

        /// <summary>
        /// Get the scores
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public async Task<IEnumerable<License>> GetScores(string jsonString)
        {
            var matches = new List<License>();

            // Deserialize request string into license object
            var searchObj = JsonConvert.DeserializeObject<License>(jsonString);

            // Deserialize jsonDB into list of license objects
            var db = JsonConvert.DeserializeObject<List<License>>(_settings.GetDB());

            // Search based on name, dob, etc.
            db.ForEach(obj =>
            {
                int matchedPropCount = 0;
                // if property matched, add to count
                if (obj.FirstName.ToLower() == searchObj.FirstName.ToLower()) matchedPropCount++;
                if (obj.LastName.ToLower() == searchObj.LastName.ToLower()) matchedPropCount++;
                if (obj.Address1.ToLower() == searchObj.Address1.ToLower()) matchedPropCount++;
                if (obj.Address2.ToLower() == searchObj.Address2.ToLower()) matchedPropCount++;
                if (obj.City.ToLower() == searchObj.City.ToLower()) matchedPropCount++;
                if (obj.State.ToLower() == searchObj.State.ToLower()) matchedPropCount++;
                if (obj.Zip == searchObj.Zip) matchedPropCount++;
                if (obj.DOB == searchObj.DOB) matchedPropCount++;

                if (matchedPropCount > 0)
                {
                    var match = new License();
                    match = obj;

                    // if the licnese number matches, the score is 100
                    match.ConfidenceScore = (obj.LicenseNo == searchObj.LicenseNo) ? 100 : matchedPropCount * 10;

                    matches.Add(match);
                }
            });

            // Return matches sorted by score
            return matches.OrderByDescending(x => x.ConfidenceScore).ToList();
        }
    }
}
