﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DealerPolicy.Developer;
using System.Text.Json;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("GetScores")]
        public async Task<IEnumerable<License>> Get([FromBody] JsonElement body)
        {
            string json = JsonSerializer.Serialize(body);
            Service service = new Service( new Settings("[{\"FirstName\": \"Jane\",\"LastName\": \"Doe\",\"DOB\": \"1993-10-17\",\"LicenseNo\": \"TJ123699\",\"Address1\": \"123 SILVER LIGHT WAY\",\"Address2\": \"APT7\",\"City\": \"Maumee\",\"State\": \"OH\",\"Zip\": \"43537\"},{\"FirstName\": \"John\",\"LastName\": \"Wilson\",\"DOB\": \"1992-10-17\",\"LicenseNo\": \"TJ123688\",\"Address1\": \"123 SILVER LIGHT DR\",\"Address2\": \"APT6\",\"City\": \"Maumee\",\"State\": \"OH\",\"Zip\": \"43537\"},{\"FirstName\": \"John\",\"LastName\": \"Doe\",\"DOB\": \"1992-10-17\",\"LicenseNo\": \"TJ123684\",\"Address1\": \"123 SILVER LIGHT WAY\",\"Address2\": \"APT4\",\"City\": \"Maumee\",\"State\": \"OH\",\"Zip\": \"43537\"},{\"FirstName\": \"Clark\",\"LastName\": \"Clarkson\",\"DOB\": \"1995-12-12\",\"LicenseNo\": \"TJ55879\",\"Address1\": \"78 WRONG WAY\",\"Address2\": \"APT95\",\"City\": \"Charlesville\",\"State\": \"OK\",\"Zip\": \"55283\"}]"));
            return await service.GetScores(json);
        }
    }
}
