using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Services.Models;

namespace DealerPolicy.Developer.Test
{
    [TestClass]
    public class ServiceTests
    {
        private string JsonDB => "[{\"FirstName\": \"Jane\",\"LastName\": \"Doe\",\"DOB\": \"1993-10-17\",\"LicenseNo\": \"TJ123699\",\"Address1\": \"123 SILVER LIGHT WAY\",\"Address2\": \"APT7\",\"City\": \"Maumee\",\"State\": \"OH\",\"Zip\": \"43537\"},{\"FirstName\": \"John\",\"LastName\": \"Wilson\",\"DOB\": \"1992-10-17\",\"LicenseNo\": \"TJ123688\",\"Address1\": \"123 SILVER LIGHT DR\",\"Address2\": \"APT6\",\"City\": \"Maumee\",\"State\": \"OH\",\"Zip\": \"43537\"},{\"FirstName\": \"John\",\"LastName\": \"Doe\",\"DOB\": \"1992-10-17\",\"LicenseNo\": \"TJ123684\",\"Address1\": \"123 SILVER LIGHT WAY\",\"Address2\": \"APT4\",\"City\": \"Maumee\",\"State\": \"OH\",\"Zip\": \"43537\"},{\"FirstName\": \"Clark\",\"LastName\": \"Clarkson\",\"DOB\": \"1995-12-12\",\"LicenseNo\": \"TJ55879\",\"Address1\": \"78 WRONG WAY\",\"Address2\": \"APT95\",\"City\": \"Charlesville\",\"State\": \"OK\",\"Zip\": \"55283\"}]";
        
        // I broke the default test :( ... not sure exactly how
        //[TestMethod]
        //public async Task MyMethodReturnsTrueIfAValueIsProvided()
        //{
        //    // Given
        //    var mockDependentService = new Mock<IDependentService>();
        //    mockDependentService.Setup(m => m.GetValueAsync()).Returns(Task.FromResult("value"));

        //    var service = new Service(mockDependentService.Object);

        //    // When
        //    var result = await service.MyMethodAsync(null).ConfigureAwait(false);

        //    // Then
        //    Assert.IsTrue(result);
        //}
        
        [TestMethod]
        public async Task TestNoMatchesAsync()
        {
            var service = new Service(new Settings(JsonDB));

            var testObj = new License
            {
                FirstName = "James",
                LastName = "Jameson",
                Address1 = "999 Test Dr",
                Address2 = "APT999",
                City = "Cincinnotacity",
                State = "XX",
                Zip = "9999",
                LicenseNo = "XX9999",
                DOB = new System.DateTime(1900, 01, 01)
            };

            List<License> result = (List<License>)await service.GetScores(JsonConvert.SerializeObject(testObj));

            Assert.IsTrue(result.Count == 0);
        }

        [TestMethod]
        public async Task TestExactMatchAsync()
        {
            var service = new Service(new Settings(JsonDB));

            var testObj = new License
            {
                FirstName = "John",
                LastName = "Doe",
                Address1 = "123 SILVER LIGHT WAY",
                Address2 = "APT4",
                City = "Maumee",
                State = "OH",
                Zip = "43537",
                LicenseNo = "TJ123684",
                DOB = new System.DateTime(1992, 10, 14)
            };

            List<License> result = (List<License>)await service.GetScores(JsonConvert.SerializeObject(testObj));

            Assert.IsTrue(result.Where(r => r.ConfidenceScore == 100).Any());
        }

        [TestMethod]
        public async Task TestPartialMatchAsync()
        {
            var service = new Service(new Settings(JsonDB));

            var testObj = new License
            {
                FirstName = "John",
                LastName = "Joe",
                Address1 = "123 SILVER LIGHT Drive",
                Address2 = "APT4",
                City = "Maumee",
                State = "OH",
                Zip = "43551",
                LicenseNo = "TJ123889",
                DOB = new System.DateTime(1998, 11, 15)
            };

            List<License> result = (List<License>)await service.GetScores(JsonConvert.SerializeObject(testObj));

            Assert.IsTrue(result.Where(r => r.ConfidenceScore > 0).Any());
        }

        [TestMethod]
        public async Task TestMatchAccuracy()
        {
            var service = new Service(new Settings(JsonDB));

            var testObj = new License
            {
                FirstName = "John", //match
                LastName = "Joe",
                Address1 = "123 SILVER LIGHT Drive",
                Address2 = "APT4", //match
                City = "Maumee", //match
                State = "OH", //match
                Zip = "43551",
                LicenseNo = "TJ123889",
                DOB = new System.DateTime(1998, 11, 15)
            };

            List<License> result = (List<License>)await service.GetScores(JsonConvert.SerializeObject(testObj));

            Assert.IsTrue(result.Where(r => r.ConfidenceScore == 40).Any());
        }
    }


}
